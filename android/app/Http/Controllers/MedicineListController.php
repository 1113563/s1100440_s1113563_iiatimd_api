<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedicineList;

class MedicineListController extends Controller
{
  public function index(){
      return MedicineList::leftJoin('medicine', 'medicineList_id', '=', 'medicine_id')->select("medicine.name")->get();

  }

  public function show($medicine){
    return MedicineList::leftJoin('medicine', 'medicineList_id', '=', 'medicine_id')->select("medicine.name", "medicine.dosage", "medicine.quantity")->where("medicine.name",$medicine)->get();
  }
}
