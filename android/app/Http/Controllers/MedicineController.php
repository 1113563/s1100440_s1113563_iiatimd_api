<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicine;
use Illuminate\Support\Facades\DB;


class MedicineController extends Controller
{
    public function index(){
      // return DB::table('medicine')->select('medicine_id','name', 'dosage', 'quantity')->get();
      return Medicine::all();

    }
}
