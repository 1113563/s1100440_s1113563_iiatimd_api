<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//show all medication names
Route::get('/medicinelist', "MedicineListController@index");

//shows specifics of one medication
Route::get('/medicinelist/{medicine}', "MedicineListController@show");

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout','AuthController@logout');
Route::get('user', 'AuthController@getAuthUser');