<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            "name" => "Bob",
            "password" => "debouwer",
            "email" => "s1113563@student.hsleiden.nl",
        ]);
    }
}
