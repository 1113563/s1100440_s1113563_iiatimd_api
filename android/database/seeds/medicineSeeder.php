<?php

use Illuminate\Database\Seeder;

class medicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("medicine")->insert([
          "name" => "Ketamine",
          "dosage" => 4.20,
          "quantity" => 69,
        ]);

        DB::table("medicine")->insert([
          "name" => "Mollie",
          "dosage" => 6.9,
          "quantity" => 420,
        ]);
    }
}
