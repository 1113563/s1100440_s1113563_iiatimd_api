<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicineListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_list', function (Blueprint $table) {
            $table->unsignedInteger("medicineList_id");
            $table->foreign('medicineList_id')->references("medicine_id")->on('medicine');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("medicine_list", function($table){
          $table->dropForeign(['medicine_id']);
        });
        Schema::dropIfExists('medicine_list');

    }
}
