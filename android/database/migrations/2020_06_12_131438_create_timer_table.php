<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timer', function (Blueprint $table) {
            $table->integer("timer_id")->primary();
            $table->String("medicine_name");
            $table->unsignedInteger("medicine_id");
            $table->foreign('medicine_id')->references("medicine_id")->on('medicine');
            $table->integer("hours");
            $table->integer("minutes");
            $table->boolean("repeatable");
            $table->boolean("medicine_taken");
            //$table->integer("frequency");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("timer", function($table){
        $table->dropForeign(['medicine_id']);
      });
        Schema::dropIfExists('timer');
    }
}
